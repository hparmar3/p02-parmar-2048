//
//  AppDelegate.h
//  Hevin-2048
//
//  Created by Hevin Parmar on 2/7/17.
//  Copyright © 2017 Hevin Parmar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

