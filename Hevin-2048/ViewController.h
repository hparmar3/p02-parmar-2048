//
//  ViewController.h
//  Hevin-2048
//
//  Created by Hevin Parmar on 2/7/17.
//  Copyright © 2017 Hevin Parmar. All rights reserved.
//

#import <UIKit/UIKit.h>
int random_num;
@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *tiles;
@property (weak, nonatomic) IBOutlet UILabel *score;


@end

