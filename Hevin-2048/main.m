//
//  main.m
//  Hevin-2048
//
//  Created by Hevin Parmar on 2/7/17.
//  Copyright © 2017 Hevin Parmar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
